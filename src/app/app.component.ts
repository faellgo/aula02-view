import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'aula02-view';

  nome: string;

  salvar(){
    console.log('executou!' + this.nome);
  }

}
